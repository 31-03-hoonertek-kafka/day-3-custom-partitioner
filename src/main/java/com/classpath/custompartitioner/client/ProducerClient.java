package com.classpath.custompartitioner.client;


import com.classpath.custompartitioner.model.Person;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class ProducerClient implements CommandLineRunner {

    private final KafkaTemplate<Long, Person> kafkaTemplate;
    @Override
    public void run(String... args) throws Exception {
        log.info("Inside the producer client :::: start");
        Person platinumCustomer = Person.builder().id(1).personType(Person.PersonType.PLATINUM).build();
        Person goldCustomer = Person.builder().id(2).personType(Person.PersonType.GOLD).build();
        Person silverCustomer = Person.builder().id(3).personType(Person.PersonType.SILVER).build();

        ProducerRecord<Long, Person> producerRecord = new ProducerRecord<>("persons-topic", platinumCustomer.getId(), platinumCustomer);
        ProducerRecord<Long, Person> goldCustomerRecord = new ProducerRecord<>("persons-topic", goldCustomer.getId(), goldCustomer);
        ProducerRecord<Long, Person> silverCustomerproducerRecord = new ProducerRecord<>("persons-topic", silverCustomer.getId(), silverCustomer);
        this.kafkaTemplate.send(producerRecord);
        this.kafkaTemplate.send(goldCustomerRecord);
        this.kafkaTemplate.send(silverCustomerproducerRecord);


    }
}
