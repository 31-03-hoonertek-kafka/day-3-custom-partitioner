package com.classpath.custompartitioner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomPartitionerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomPartitionerApplication.class, args);
    }

}
