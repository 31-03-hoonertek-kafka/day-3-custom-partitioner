package com.classpath.custompartitioner.paritioner;


import com.classpath.custompartitioner.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class CustomPartitioner implements Partitioner {

    @Override
    public int partition(String topic, Object key, byte[] bytes, Object value, byte[] valueBytes, Cluster cluster) {
        log.info("Came inside the CustomPartioner method :: ");
        List<PartitionInfo> partitionInfos = cluster.availablePartitionsForTopic(topic);
        int totalParitions = partitionInfos.size();
        log.info("The number of partitions :: {}", totalParitions);
        Person payload = (Person)value;
        if (payload.getPersonType() == Person.PersonType.PLATINUM.PLATINUM){
            return 1;
        } else if (payload.getPersonType() == Person.PersonType.GOLD){
            return 2;
        }

        //perform the mod opeartor on the number of partitions
        return 0;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
