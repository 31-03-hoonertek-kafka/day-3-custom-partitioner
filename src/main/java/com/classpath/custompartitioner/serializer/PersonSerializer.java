package com.classpath.custompartitioner.serializer;

import com.classpath.custompartitioner.model.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class PersonSerializer  implements Serializer<Person> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, Person person) {
        if (person == null) {
            return null;
        }
        try {
            return objectMapper.writeValueAsBytes(person);
        } catch (Exception e) {
            throw new SerializationException("Error serializing JSON message", e);
        }
    }

    @Override
    public byte[] serialize(String topic, Headers headers, Person person) {
        if (person == null) {
            return null;
        }
        try {
            return objectMapper.writeValueAsBytes(person);
        } catch (Exception e) {
            throw new SerializationException("Error serializing JSON message", e);
        }
    }

    @Override
    public void close() {

    }
}
