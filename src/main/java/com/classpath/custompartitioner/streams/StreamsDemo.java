package com.classpath.custompartitioner.streams;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class StreamsDemo {

    public static void main(String[] args) {
        //input -> value, output -> boolean
/*
        Predicate<String> stringPredicate = property ->   property.equalsIgnoreCase("kafka");
        System.out.println(stringPredicate.test("hello-world"));
        System.out.println(stringPredicate.test("KaFka"));
*/
        List<Product> phones = Arrays.asList(
                Product.builder().name("IPhone-13").price(63_000).company(Company.APPLE).build(),
                Product.builder().name("IPhone-12").price(50_000).company(Company.APPLE).build(),
                Product.builder().name("Galaxy").price(55_000).company(Company.SAMSUNG).build(),
                Product.builder().name("X-Pro").price(25_000).company(Company.LG).build(),
                Product.builder().name("S-12").price(65_000).company(Company.SAMSUNG).build()
        );

        Predicate<Product> isPriceGt50k = product -> product.getPrice() >= 50_000;
        Predicate<Product> isIphone13 = product -> product.getName().equalsIgnoreCase("IPhone-13");
        Predicate<Product> isIphone12 = product -> product.getName().equalsIgnoreCase("IPhone-12");
        Predicate<Product> isAppleProduct = isIphone12.or(isIphone13);
        Predicate<Product> isNotAppleProduct = isAppleProduct.negate();

        Predicate<Product> notIPhoneAndPriceGt50k = isNotAppleProduct.and(isPriceGt50k);

        //Function
        Function<Product, String> productToNameMapper = Product::getName;
        Function<Product, Double> productToPriceMapper = Product::getPrice;
        Function<Product, Company> productToCompany = Product::getCompany;
        Function<String, String> productNameMapper = name -> "Product name:: " + name;

        //Consumer
        Consumer<String> printProductName = System.out::println;


        phones
                .stream()
                .filter(notIPhoneAndPriceGt50k)
                .map(productToNameMapper)
                .map(String::toUpperCase)
                .map(productNameMapper)
                .forEach(printProductName);


    }

}

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
class Product {
    private String name;
    private double price;
    private Company company;
}
enum Company {
    APPLE,
    SAMSUNG,
    LG,
    MOTOROLA
}
