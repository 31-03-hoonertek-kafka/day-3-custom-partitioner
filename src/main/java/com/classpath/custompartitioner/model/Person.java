package com.classpath.custompartitioner.model;

import lombok.*;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private long id;
    public enum PersonType {
            PLATINUM,
            SILVER,
            GOLD
    }
    private PersonType personType;
}


